---
title: "Taskfile for PHP developers"
date: 2021-05-16T10:13:34+02:00
tags: ["Taskfile", "PHP"]
description: "You always liked how `artisan`/`composer` solve things but it is too much to type?"
showToc: true
author: "Czékus Máté"
draft: false
hidemeta: false
comments: false
---
# TLDR; on Taskfile
Taskfile is a task runner / build tool.

It is meant to be a more modern alternative to `GNU Make`, it uses YAML as its primary configuration language.

Since it is written in Go(_lang_), it can be compiled literally any platform as a static binary.
## Install
You can install Taskfile on Mac systems with `brew`:
```sh
brew install go-task/tap/go-task
```
On Linux systems there is a handy AUR package for the Arch guys, and there is also a Snap package for...
I don't know who it is for, but there must people who can use it since it is enabled and installed by default on Ubuntu systems. (Right Canonical?)

So at the end of the day, I had to use the good, old `wget`, `tar` combo:
```bash
wget https://github.com/go-task/task/releases/download/v3.4.2/task_linux_amd64.tar.gz -O - | tar -xzvf - task
sudo mv task /usr/local/bin/task
sudo chmod +x /usr/local/bin/task
```
# _"So what can we do with that?"_
Well, I glad you asked my friend, we can do almost everything.

But first, before I show you how to do some advanced stuff lets just make a little `hello-world`, 
since our great planet hasn't been greeted for a while now.
## Hello world!
To create a `Taskfile.yml` just run the following command:
```bash
task --init
```
not bad, right? The new file already contains a `hello-world` example.
```yaml
# https://taskfile.dev

version: '3'

vars:
  GREETING: Hello, World!

tasks:
  default:
    cmds:
      - echo "{{.GREETING}}"
    silent: true
```
As you can see there are `vars` acting as global variables in our task file.
And `tasks`, those guys are the commands that we can execute.

The `default` keyword means it is the default command to run when you simply run the `task` command in that directory.

_"But I don't belive to any configuration file!"_ you might say, **so lets verify that:**
```bash
task
```
_"Okay but what if I want to greet my friend John?"_, **say no more!**
```bash
task GREETING="Hello, John!"
```
Now let me explain what happened, In the `Taskfile.yml` we have a variable with the name `GREETING` and we just used Taskfile's variable override feature.
## Advanced stuff
### Features
First of all, I have to mention some killer feature, before we move forward,
since I don't want to write a 2. documentation for Taskfile, because the 1. is already well written.
- [Environment variable support, with `.env` files.](https://taskfile.dev/#/usage?id=environment-variables)
- [Multiple task files with OS specific ones.](https://taskfile.dev/#/usage?id=including-other-taskfiles)
- [10/10 templating engine with all the feature you can ask for.](https://taskfile.dev/#/usage?id=gos-template-engine)
- [Default `--list` command where you can document your commands (promise me to do that please!)](https://taskfile.dev/#/usage?id=help)

### Examples
So lets start with a simple template jugling.

Imagine you have a private composer repository hosted on gitlab.com (cause I do 😃)
and want to create a command to create the `auth.json` file for composer.

- First of all we create a command in our `Taskfile.yml`:
```yaml
# https://taskfile.dev

version: '3'

vars:
  GREETING: Hello, World!

tasks:
  default:
    cmds:
      - echo "{{.GREETING}}"
    silent: true
  
  generate-token:
    silent: true
    cmds:
      -
```
- Now we want to document our command:
```yaml
  generate-token:
    silent: true
    desc: |
      Generates an 'auth.json' file to the project's root folder..
      PLEASE MAKE SURE YOU PASS DOWN A TOKEN TO THE 'TOKEN' VARIABLE!
    cmds:
      -
```
Oh, yes we will need local variables, it is the same as the root `vars` flag,
the only exception is this variables are scoped to this command only.
- Create the variables and some basic echo-ing.
```yaml
  generate-token:
    silent: true
    desc: |
      Generates an 'auth.json' file to the project's root folder..
      PLEASE MAKE SURE YOU PASS DOWN A TOKEN TO THE 'TOKEN' VARIABLE!
    vars:
      JSON_TEMPLATE: '{\"gitlab-token\": {\"gitlab.com\": \"TOKEN\"}}'
    cmds:
      - 'echo Generating Composer auth.json file with token: {{.TOKEN}}'
      - 'echo Tokens created successfuly!'
```
- Replace the TOKEN word in the `JSON_TEMPLATE` to our token
```yaml
  generate-token:
    silent: true
    desc: |
      Generates an 'auth.json' file to the project's root folder..
      PLEASE MAKE SURE YOU PASS DOWN A TOKEN TO THE 'TOKEN' VARIABLE!
    vars:
      JSON_TEMPLATE: '{\"gitlab-token\": {\"gitlab.com\": \"TOKEN\"}}'
    cmds:
      - 'echo Generating Composer auth.json file with token: {{.TOKEN}}'
      - 'echo {{.JSON_TEMPLATE | replace "TOKEN" .TOKEN}} > auth.json'
      - 'echo Tokens created successfuly!'
```
Now run `task generate-token TOKEN=PWN_ME_123`

_**"Wow, thats cool, but what happened?"**_

Well, we just echo our template to an `auth.json` file, while using the 
[`replace` function from `slim-sprig` library](https://go-task.github.io/slim-sprig/strings.html) 
to replace the `TOKEN` word to our `TOKEN` variable.

**For the second example let me give you the code first and the I'll explain it step-by-step**

**The problem:**
Lets pretend for a minute that you are the type of guy,
who don't like to install `composer` and `php` or `npm` and `node` to your host machine, like myself.

But you really need those dependencies because you don't like when the code highlighting is yelling at you visually.

Or PHPStorm's autocompletion doesn't work as it should (thats why you paying yearly to JetBrains, cmon' man...)

**The code:**
```yaml
  install-deps:
    silent: true
    deps: [generate-token]
    desc: |
      Install all of the project dependencies to your local machine.
      PLEASE MAKE SURE YOU PASS DOWN A TOKEN TO THE 'TOKEN' VARIABLE!
    cmds:
      - 'echo Installing Composer dependencies...'
      - 'docker run --rm -v $PWD:/app -w /app composer:2 composer install --ignore-platform-reqs'
      - 'echo Installing NPM dependencies...'
      - 'docker run --rm -v $PWD:/app -w /app node:lts npm install'
      - 'echo Dependencies installed successfuly!'
```
_"There are only one new stanza, I can work with that, so what it does?"_

It makes your command to run `task generate-token` before running the `cmds`, 
since your `compose install` will likely to fail, without that `auth.json` file.

# Summary
At the end of the day your `Taskfile.yml` looks like this:
```yaml
# https://taskfile.dev

version: '3'

vars:
  GREETING: Hello, World!

tasks:
  default:
    cmds:
      - echo "{{.GREETING}}"
    silent: true
  
  generate-token:
    silent: true
    desc: |
      Generates an 'auth.json' file to the project's root folder..
      PLEASE MAKE SURE YOU PASS DOWN A TOKEN TO THE 'TOKEN' VARIABLE!
    vars:
      JSON_TEMPLATE: '{\"gitlab-token\": {\"gitlab.com\": \"TOKEN\"}}'
    cmds:
      - 'echo Generating Composer auth.json file with token: {{.TOKEN}}'
      - 'echo {{.JSON_TEMPLATE | replace "TOKEN" .TOKEN}} > auth.json'
      - 'echo Tokens created successfuly!'

  install-deps:
    silent: true
    deps: [generate-token]
    desc: |
      Install all of the project dependencies to your local machine.
      PLEASE MAKE SURE YOU PASS DOWN A TOKEN TO THE 'TOKEN' VARIABLE!
    cmds:
      - 'echo Installing Composer dependencies...'
      - 'docker run --rm -v $PWD:/app -w /app composer:2 composer install --ignore-platform-reqs'
      - 'echo Installing NPM dependencies...'
      - 'docker run --rm -v $PWD:/app -w /app node:lts npm install'
      - 'echo Dependencies installed successfuly!'
```

I hope you learned something or I just got you intersted in the Taskfile project.

In any case check out the [Taskfile's documentation](https://taskfile.dev/#/) there is waaaay more option and example there. 
