---
title: "About me"
url: "/about"
showToc: false
---

# Czékus Máté

I'm a mid-level software developer and devops guy based on Budapest.

Currently, I'm at [Cyclick](https://cyclick.hu/) lead the development of
the [iLogistic platform](https://api.ilogistic.eu/documentation/) almost 2 years for now.

That involves **a lot** of PHP and Go microservice, primarily solving problems fulfillment and e-commerce workflow.
I wrote everything from a small email service, to complex heavily async queue based multiservice webshop to warehouse
adapter mechanisms, serving hundreds of integrations.

Most of the time I also do work on some DevOps related stuff, like building CI pipelines with Gitlab, maintaining and
querying an on-prem [LGTM stack (Loki, Grafana, Tempo, Mimir)](https://github.com/grafana#lgtm=) or configuring bare
metal network hardware.

Before that I was at [Jura](https://jura.hu/) working on cloud migrations and on a backend for the digital security
department. Here I mostly worked on a Docker Swarm cluster running on AWS and ported legacy monolith backends to
microservices using PHP.

In my free time I'm usually hacking on [arkade](https://github.com/alexellis/arkade) with my cat.
![debug-cat](/blog/debug-cat.webp)
Or optimizing speciality filter coffee like it's a software.
![brew-kit](/blog/brew-kit.webp)
_(If you made this far and know how to collect metrics form an acaia scale with Prometheus DM me please! :D)_